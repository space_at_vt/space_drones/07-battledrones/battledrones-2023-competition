import rospy
from std_msgs.msg import String
from sensor_msgs.msg import CompressedImage

class Forwarder():
	def __init__(self):
		self.pub = rospy.Publisher('/tag_result_out/pose_auth', String, queue_size=10)

		self.listen()

	def secure(self, tag_pose):
		tag_pose_secured = tag_pose # Add code to secure pose data here

		self.pub.publish(tag_pose_secured)

	def listen(self):
		rospy.Subscriber('/tag_result_out/pose', String, self.secure)
		rospy.spin()

def main():
	rospy.init_node('BD_1_apriltag_forwarder', anonymous=True)

	apriltag_forwarder = Forwarder()

if __name__ == '__main__':
	main()