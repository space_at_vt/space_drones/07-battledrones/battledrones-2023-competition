#!/usr/bin/env python
# Third Party Imports
from telnetlib import SSPI_LOGON
import rospy
import time
from numpy import array, array, sin, cos, where, radians, arcsin, arctan2
from numpy.linalg import norm
import numpy as np
from tf import transformations
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import Point, PoseStamped, Quaternion, PolygonStamped, TwistStamped
from std_msgs.msg import String, Bool

# from gazebo_msgs.msg import ModelStates
from mavros_msgs.msg import PositionTarget
from mavros_msgs.srv import CommandBool
from mavros_msgs.srv import SetMode

# Class to be used for autonomous drone navigation using object oriented programming format

class navigator:
    def __init__(self, drone_name, setpoint_mode):
        # Publisher for sending Position & Yaw Setpoints to drone
        self.pose_setpoint_pub = rospy.Publisher('/'+drone_name+'/setpoints', String, queue_size=10)

        # Publisher for sending True/False command to launch/land drone
        self.fly_pub = rospy.Publisher('/'+drone_name+'/fly', Bool, queue_size=10)

        # Subscriber for AprilTag Position and ID number
        rospy.Subscriber('/tag_result_out/pose', String, self.tag_pose_callback)

        # Subscriber for Drone's position in local East/North/Up Coordinate Frame
        rospy.Subscriber('/'+drone_name+'/local_position/pose', PoseStamped, self.drone_pose_callback)

        # Subscriber for Drone's velocity
        rospy.Subscriber('/'+drone_name+'/local_position/velocity_local', TwistStamped, self.drone_vel_callback)

        # Global Class Variables of message format for data storage
        self.drone_pose = PoseStamped()
        self.drone_vel = TwistStamped()

        self.setpoint_mode = setpoint_mode

        # Set ROS frequency in Hz for use in loops -> self.rate.sleep()
        self.ROS_Freq = 25
        self.rate = rospy.Rate(self.ROS_Freq)
        time.sleep(2)

    #*** move_to ***#
    # This function takes a commanded x/y/z/yaw and sends it to 
    # BD_Mavros_coms to be executed using the mode specified in the 
    # init function
    def move_to(self, x, y, z, yaw):
        self.set_x = x
        self.set_y = y
        self.set_z = z
        self.set_yaw = yaw
        self.pose_setpoint_pub.publish(String("%s %s %s %s %s" %(self.setpoint_mode, x, y, z, yaw)))

    #*** fly ***#
    # This function will tell BD_Mavros_coms to launch (True) and
    # to return to home and land (False)
    def fly(self, fly_veh):
        self.fly_pub.publish(fly_veh)
    
    #*** drone_pose_callback ***#
    # This function will be called whenever new pose data from the
    # drone is available
    def drone_pose_callback(self, pose):
        self.drone_pose = pose

    #*** drone_pose_callback ***#
    # This function will be called whenever new velocity data from the
    # drone is available
    def drone_vel_callback(self, twist):
        self.drone_vel = twist

    #*** tag_pose_callback ***#
    # This function is called whenever new tag data is available. The
    # tag data is formatted as a space seperated string where the entries
    # are as follows; all coordinates are in local ENU frame:
    #   1. Tag ID
    #   2. Tag X position
    #   3. Tag Y position
    #   4. Tag Z position
    #   5. Tag Yaw
    #   5. Timestamp of when tag image was captured
    def tag_pose_callback(self,msg):
        data = str(msg.data).split()
        self.gate_num = int(data[0])
        self.gate_pose_x = float(data[1])
        self.gate_pose_y = float(data[2])
        self.gate_pose_z = float(data[3])
        self.gate_pose_yaw = float(data[4])
        self.gate_timestamp = float(data[5])


def main():
    # Initialize the ROS Node
    rospy.init_node('BD_1_navigator', anonymous=True)

    # Create an instance of the object class navigator. BD_1 is the node name 
    # for working with the drone and mavros. The setpoint mode is the mode in
    # which setpoints are used by BD_mavros_coms:
    #   Mode 0 is position and yaw setpoints in ENU coordinate frame. 
    #   Mode 1 is x/y/z velocity setpoints and yawrate setpoints. 
    #   Mode 2 is position based setpoints that are rotated around the up axis 
    #       by the amount specified in drone_config.yaml.
    drone_nav = navigator('BD_1', 0)

    #*** Launch Vehicle *************************************#
    drone_nav.fly(True)
    print("Vehicle Flying")     # Print Data to terminal
    time.sleep(15)              # Sleep for 15 seconds
    
    #*** Fly to a point x = 1, y = 0, z = 1.5, yaw = 0 ******#
    drone_nav.move_to(1, 0, 1.5, 0)
    time.sleep(10)              # Sleep for 10 seconds

    #*** Return home and land *******************************#
    drone_nav.fly(False)

    #********************************************************#
    

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
