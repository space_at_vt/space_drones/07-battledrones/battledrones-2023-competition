#!/bin/sh

# Set Session Name
SESSION="BattleDronesBoot"
SESSIONEXISTS=$(tmux list-sessions | grep $SESSION)

# Only create tmux session if it doesn't already exist
if [ "$SESSIONEXISTS" = "" ]
then
	# Start New Session with our name
	tmux new-session -d -s $SESSION

	# Name first Pane and split into multiple
	tmux rename-window -t 0 'BattleDrones startup'

	tmux select-pane -t 0
	tmux split-window -h
	tmux split-window -v   
	tmux select-pane -t 0
	tmux split-window -v


	# Send commands to pane 0
    # Send commands to pane 1

	tmux send-keys -t 0 'sshpass -p battledrone ssh battledrone@192.168.20.176' Enter
	tmux send-keys -t 1 'sshpass -p battledrone ssh battledrone@192.168.20.176' Enter
    tmux send-keys -t 2 'sshpass -p battledrone ssh battledrone@192.168.20.176' Enter
    tmux send-keys -t 3 'sshpass -p battledrone ssh battledrone@192.168.20.176' Enter
    sleep 4
    tmux send-keys -t 0 'roslaunch mavros BD.launch' Enter
    sleep 4
    tmux send-keys -t 1 'roslaunch usb_cam usb_cam-test.launch' Enter
    tmux send-keys -t 2 'cd BattleDrones/src' Enter
    tmux send-keys -t 3 'cd BattleDrones/src' Enter
    sleep 4
    tmux send-keys -t 2 'python BD_mavros_coms.py'
    tmux send-keys -t 3 'python apriltag_read.py' Enter
    

fi

# Attach Session, on the Main window
tmux attach-session -t $SESSION:0
