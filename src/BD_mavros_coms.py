#!/usr/bin/env python
# Third Party Imports
import rospy
import time
import yaml
from numpy import array, array, sin, cos, where, radians, arcsin, arctan2
from numpy.linalg import norm
import numpy as np
from tf import transformations
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from geometry_msgs.msg import Point, PoseStamped, Quaternion, PolygonStamped, TwistStamped
from std_msgs.msg import String, Bool

# from gazebo_msgs.msg import ModelStates
from mavros_msgs.msg import PositionTarget
from mavros_msgs.srv import CommandBool, CommandHome
from mavros_msgs.srv import SetMode

def main():
    rospy.init_node('BD_1_mavros_coms', anonymous=True)

    with open("drone_config.yaml", 'r') as stream:
        data_loaded = yaml.safe_load(stream)

    drone_name = str(data_loaded['drone_name'])
    launch_height = float(data_loaded['launch_height'])
    RTL_height = float(data_loaded['RTL_height'])
    control_rate = int(data_loaded['control_rate'])
    flight_time = int(data_loaded['flight_time'])

    # Initiate Flight Controller for drone (drone_name, controller_rate[Hz], return to land height[m])
    drone = Flight_controller(drone_name, control_rate, RTL_height)
    drone.setOffboardMode()
    while not rospy.is_shutdown():
        if drone.fly:
            drone.launch(launch_height)     # Launch drone to _x_ height
            drone.hold(flight_time)      # Drone has _x_ seconds (5 and a half minutes) to perform flight and then will return to land
        drone.rate.sleep()

class Flight_controller():
    def __init__(self, node_name, loop_freq, RTL_height):
        self.node_name = node_name
        # A Message for the current local position of the drone
        self.current = PositionTarget()
        self.init_position = PositionTarget()
        self.pose_error = PositionTarget()
        self.rotated_position = PoseStamped()
        self.target =  PositionTarget()

        self.target.coordinate_frame = self.target.FRAME_LOCAL_NED

        # Drone park long direction is rotated ~140 degrees from due East
        self.drone_park_rot = 140*np.pi/180

        self.RTL_height = RTL_height
	
	    # NOTE: self.measurement is recorded in the right/forward/up frame
        self.vision_pose =  PoseStamped()

        self.drone_vel = TwistStamped()

        ### Trajectory Publishers ###
        # raw setpoint publisher
        self.pose_publish = rospy.Publisher('/'+self.node_name+'/setpoint_raw/local', PositionTarget, queue_size=10)
        self.rotated_pose_publish = rospy.Publisher('/'+self.node_name+'/local_position/drone_park', PoseStamped, queue_size = 10)

        # Local pose subscriber, to check when vehicle reaches a setpoint
        rospy.Subscriber('/'+self.node_name+'/local_position/pose', PoseStamped, self.poseCallback)
        rospy.Subscriber('/'+self.node_name+'/local_position/velocity_local', TwistStamped, self.drone_vel_callback)
        rospy.Subscriber('/'+self.node_name+'/setpoints', String, self.setpointCallback)
        rospy.Subscriber('/'+self.node_name+'/fly', Bool, self.flyCallback)

        self.fly = False
        self.flying = False

        # Set publish/subscribe rate
        self.rate = rospy.Rate(loop_freq)
        self.loop_rate = loop_freq
        self.target_height = 0
        time.sleep(2)

    def launch(self, launch_height):
        self.arm()
        time.sleep(1)
        rospy.loginfo('Drone is launching')
        self.target.type_mask = self.target.IGNORE_VX | self.target.IGNORE_VY | self.target.IGNORE_VZ | \
                                self.target.IGNORE_AFX | self.target.IGNORE_AFY | self.target.IGNORE_AFZ | \
                                self.target.IGNORE_YAW_RATE
        self.target_height = launch_height
        self.init_position.position.x = self.current.position.x
        self.init_position.position.y = self.current.position.y
        self.init_position.position.z = self.current.position.z            # Hardcode to 15 cm because height measurement will be inacurate 
                                                        # until drone gets above range sensor minimum,
                                                        # ~15 cm due to imu drift
        self.init_position.yaw = self.current.yaw
        self.target.position.x = self.init_position.position.x
        self.target.position.y = self.init_position.position.y
        self.target.position.z = self.current.position.z + self.target_height

        self.target.yaw = self.init_position.yaw
        # rospy.loginfo(self.target.position)
        time.sleep(1)
        self.send_setpoint()
        while(abs(self.pose_error.position.x) + abs(self.pose_error.position.y) + abs(self.pose_error.position.z) > 0.1):
            # rospy.loginfo(self.pose_error.position)
            self.send_setpoint()
            self.rate.sleep()
        rospy.loginfo('Drone has reached launch position')
        self.flying = True

    def land(self, landing_rate):
        self.flying = False
        self.target.type_mask = self.target.IGNORE_VX | self.target.IGNORE_VY | self.target.IGNORE_VZ | \
                                self.target.IGNORE_AFX | self.target.IGNORE_AFY | self.target.IGNORE_AFZ | \
                                self.target.IGNORE_YAW_RATE
        rospy.loginfo('Drone is climbing to RTL height')
        self.target.position.x = self.current.position.x
        self.target.position.y = self.current.position.y
        self.target.position.z = self.RTL_height + self.init_position.position.z
        self.target.yaw = self.init_position.yaw
        self.send_setpoint()
        self.rate.sleep()
        while(abs(self.pose_error.position.z) > 0.1):
            self.send_setpoint()
            self.rate.sleep()
        
        rospy.loginfo('Drone is returning to launch location')
        self.target.position.x = self.init_position.position.x
        self.target.position.y = self.init_position.position.y
        self.send_setpoint()
        self.rate.sleep()
        time.sleep(1)
        while(abs(self.pose_error.position.x) + abs(self.pose_error.position.y) + abs(self.pose_error.position.z) > 0.1):
            self.send_setpoint()
            self.rate.sleep()
        
        rospy.loginfo('Drone is landing')
        while(not rospy.is_shutdown()):
            self.target.position.z = self.target.position.z - landing_rate/self.loop_rate
            self.send_setpoint()
            self.rate.sleep()
            if(self.target.position.z < self.init_position.position.z and abs(self.drone_vel.twist.linear.z) < 0.1):
                break
        rospy.loginfo('Drone has landed. Disarming now')
        self.disarm()
        time.sleep(1)
        rospy.loginfo('Drone is on pad and disarmed.')

    def send_setpoint(self):
        self.pose_publish.publish(self.target)

    def hold(self, hold_time):
        count = 0
        print('Holding for %s seconds' %hold_time)
        count = 0
        while(not rospy.is_shutdown()):
            self.send_setpoint()
            self.rate.sleep()
            count+=1
            if (count > hold_time*self.loop_rate):
                break
            if not self.fly:
                break
        self.land(0.25)

    def break_hold(self):
        self.break_loop = True
    
    def poseCallback(self, msg):
        self.current.position.x = msg.pose.position.x
        self.current.position.y = msg.pose.position.y
        self.current.position.z = msg.pose.position.z
        orientation = msg.pose.orientation
        quat = (orientation.x, orientation.y, orientation.z, orientation.w)
	
        self.roll, self.pitch, self.current.yaw = transformations.euler_from_quaternion(quat)

        self.pose_error.position.x = self.target.position.x - self.current.position.x
        self.pose_error.position.y = self.target.position.y - self.current.position.y
        self.pose_error.position.z = self.target.position.z - self.current.position.z

        self.pose_error.yaw = self.target.yaw - self.current.yaw


        self.rotate_frame()

    def rotate_frame(self):
        c, s = np.cos(self.drone_park_rot), np.sin(self.drone_park_rot)
        R = np.array([[c,-s],[s,c]])
        # Translate origin to drone starting position and rotate frame to align with drone cage
        xy = R.dot(np.array([[self.current.position.x - self.init_position.position.x],
                             [self.current.position.y - self.init_position.position.y]]))

        self.rotated_position.pose.position.x = float(xy[0])
        self.rotated_position.pose.position.y = float(xy[1])
        self.rotated_position.pose.position.z = self.current.position.z
        
        delta_yaw = self.current.yaw - self.drone_park_rot
        yaw_rot = np.unwrap([delta_yaw])
        orientation = quaternion_from_euler(self.roll, self.pitch, yaw_rot)

        self.rotated_position.pose.orientation.x = float(orientation[0])
        self.rotated_position.pose.orientation.y = float(orientation[1])
        self.rotated_position.pose.orientation.z = float(orientation[2])
        self.rotated_position.pose.orientation.w = float(orientation[3])

        self.rotated_pose_publish.publish(self.rotated_position)

    def arm(self):
        rospy.wait_for_service('/'+self.node_name+'/cmd/arming')
        try:
            armService = rospy.ServiceProxy('/'+self.node_name+'/cmd/arming', CommandBool)
            armService(True)
        except rospy.ServiceException:
            rospy.loginfo("Service arming call failed")

    def disarm(self):
        try:
            armService = rospy.ServiceProxy('/'+self.node_name+'/cmd/arming', CommandBool)
            armService(False)
        except rospy.ServiceException:
            rospy.loginfo("Service disarming call failed")
        
    def setOffboardMode(self):
        # Position (through velocity structure) and acceleration setpoints
        self.target.position.x = 0
        self.target.position.y = 0
        self.target.position.z = 0
        self.target.yaw = 0

        for k in range(50):
            self.send_setpoint()
            self.rate.sleep()
        change_mode = rospy.ServiceProxy('/'+self.node_name+'/set_mode', SetMode)
        try:
            # base_mode = 0
            # custom_mode = 'OFFBOARD'
            out = change_mode(custom_mode='OFFBOARD')
            if out:
                rospy.loginfo("Successfully changed to offboard mode")
            else:
                rospy.spin()
        except rospy.ServiceException:
            rospy.loginfo("Service call failed")

    def setpointCallback(self, msg):
        # Only update target position or velocity if the vehicle is currently flying
        if self.flying:
            data = str(msg.data).split()
            self.flight_mode = int(data[0])
            # Position Control Mode (default)
            if self.flight_mode == 0:
                self.target.position.x = float(data[1])
                self.target.position.y = float(data[2])
                self.target.position.z = float(data[3])
                self.target.yaw = float(data[4])
                self.target.type_mask = self.target.IGNORE_VX | self.target.IGNORE_VY | self.target.IGNORE_VZ | \
                                        self.target.IGNORE_AFX | self.target.IGNORE_AFY | self.target.IGNORE_AFZ | \
                                        self.target.IGNORE_YAW_RATE
                print('Fly to %s, %s, %s' %(float(data[1]), float(data[2]), float(data[3])))
            # Velocity Control Mode
            elif self.flight_mode == 1:
                self.target.velocity.x = float(data[1])
                self.target.velocity.y = float(data[2])
                self.target.velocity.z = float(data[3])
                self.target.yaw_rate = float(data[4])
                self.target.type_mask = self.target.IGNORE_PX | self.target.IGNORE_PY | self.target.IGNORE_PZ | \
                                        self.target.IGNORE_AFX | self.target.IGNORE_AFY | self.target.IGNORE_AFZ | \
                                        self.target.IGNORE_YAW
            # Position control with using rotated x/y
            elif self.flight_mode == 2:
                x_rel = float(data[1])
                y_rel = float(data[2])
                yaw_rel = float(data[4])
                yaw_act = self.drone_park_rot + yaw_rel
                self.target.yaw = np.unwrap([yaw_act])

                c, s = np.cos(self.drone_park_rot), np.sin(self.drone_park_rot)
                R = np.array([[c,-s],[s,c]])
                xy = R.dot(np.array([[x_rel],[y_rel]]))

                self.target.position.x = float(xy[0] + self.init_position.position.x)
                self.target.position.y = float(xy[1] + self.init_position.position.y)
                self.target.position.z = float(data[3])
                
                self.target.type_mask = self.target.IGNORE_VX | self.target.IGNORE_VY | self.target.IGNORE_VZ | \
                                        self.target.IGNORE_AFX | self.target.IGNORE_AFY | self.target.IGNORE_AFZ | \
                                        self.target.IGNORE_YAW_RATE
                print('Fly to %s, %s, %s' %(float(data[1]), float(data[2]), float(data[3])))
    def flyCallback(self, data):
            self.fly = data.data

    def drone_vel_callback(self, twist):
        self.drone_vel = twist

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
    
