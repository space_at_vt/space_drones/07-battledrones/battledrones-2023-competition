#!/usr/bin/env python

### Python Spacedrone - GUI Testing
#
# Author: Minzhen Du
# Year: 2021
# Coded for SpaceDrones Lab for Space@VT
#

## %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
## %%%%%% PYTHON LIBRARIES
import os
import sys
import math
import numpy as np
import time
import datetime
import threading
import tf
import message_filters
import yaml
# from pyzbar import pyzbar
import apriltag
from tf import transformations
from tf2_msgs.msg import TFMessage

## %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
## %%%%%% IMAGE LIBRARIES
import cv2
from PIL import Image as Img
from PIL import ImageTk
from PIL import ImageDraw
from PIL import *
from cv_bridge import CvBridge

## %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
## %%%%%% ROS LIBRARIES
import rospy
from sensor_msgs.msg import CompressedImage, Image
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import String, Float64, Empty
from std_srvs.srv import Trigger
from tf2_msgs.msg import TFMessage

class AprilTagLocator():
	def __init__(self):
		pass

	def init(self):
		self.image_comp_pub = rospy.Publisher('/tag_result_out/image_raw/compressed', CompressedImage, queue_size=1)
		self.qr_pose_pub = rospy.Publisher('/tag_result_out/pose', String, queue_size = 1)

		# Apply a message filter to time synchronize camera and drone pose data
		# Real world
		image_sub = message_filters.Subscriber('/usb_cam/image_raw', Image)
		pose_sub = message_filters.Subscriber('/BD_1/local_position/pose', PoseStamped)

		ts = message_filters.ApproximateTimeSynchronizer([image_sub, pose_sub], queue_size=100, slop=0.01)
		ts.registerCallback(self.img_callback)

		with open("drone_config.yaml", 'r') as stream:
			data_loaded = yaml.safe_load(stream)

		options = apriltag.DetectorOptions(families='tag36h11',
                                 border=1,
                                 nthreads=4,
                                 quad_decimate=1.0,
                                 quad_blur=0.0,
                                 refine_edges=True,
                                 refine_decode=False,
                                 refine_pose=False,
                                 debug=False,
                                 quad_contours=True)

		self.detector = apriltag.Detector(options = options)
		
		# Constant parameters for nPn solver
		tag_size = data_loaded['tag_size']
		self.tag_points_3d = np.array([(-tag_size/2, tag_size/2, 0.0), 
									   (tag_size/2, tag_size/2, 0.0), 
									   (tag_size/2, -tag_size/2, 0.0), 
									   (-tag_size/2, -tag_size/2, 0.0)], 
									   dtype = "double")
		self.distortion_coef = np.array(data_loaded['distortion_coefs'], dtype = "double")
		self.camera_matrix = np.array(data_loaded['camera_matrix'], dtype = "double")

		# Camera offsets relative to drone
		self.camX_offset = data_loaded['camera_x']			# Meters
		self.camY_offset = data_loaded['camera_y']			# Meters
		self.camZ_offset = data_loaded['camera_z']			# Meters
		self.cam_corr_roll = data_loaded['camera_roll']		# Radians
		self.cam_corr_pitch = data_loaded['camera_pitch']	# Radians
		self.cam_corr_yaw = data_loaded['camera_yaw']		# Radians
		
		self.live_img = None

		loop_rate = rospy.Rate(int(data_loaded['image_eval_freq']))

		while True:
			if self.live_img is not None:
				self.decode()	
			loop_rate.sleep()


	def img_callback(self, img_data, Posedata):

		# Import Image Data
		self.live_img = CvBridge().imgmsg_to_cv2(img_data)
		self.img_time = img_data.header.stamp.secs + img_data.header.stamp.nsecs/1e9
		
		# Import drone pose data
		self.pos_x = Posedata.pose.position.x
		self.pos_y = Posedata.pose.position.y
		self.pos_z = Posedata.pose.position.z

		self.attx = Posedata.pose.orientation.x
		self.atty = Posedata.pose.orientation.y
		self.attz = Posedata.pose.orientation.z
		self.attw = Posedata.pose.orientation.w

		quat = (self.attx, self.atty, self.attz, self.attw)
		self.eulerRoll, self.eulerPitch, self.eulerYaw = transformations.euler_from_quaternion(quat)

		self.pos_clock = Posedata.header.stamp.secs + Posedata.header.stamp.nsecs/1e9

	def decode(self):
		self.camX = self.pos_x + self.camX_offset
		self.camY = self.pos_y + self.camY_offset
		self.camZ = self.pos_z + self.camZ_offset

		self.camRoll = self.eulerRoll + self.cam_corr_roll
		self.camPitch = self.eulerPitch + self.cam_corr_pitch
		self.camYaw = self.eulerYaw + self.cam_corr_yaw	

		if(abs(self.img_time - self.pos_clock) < 0.05):
			data = cv2.cvtColor(self.live_img, cv2.COLOR_RGB2GRAY)
			# print(self.pos_clock)
			#data = self.live_img
			#mask = cv2.inRange(data,(0,0,0),(220,220,220))
			#inverted = 255-mask
			tags = self.detector.detect(data)
			#data = inverted

			self.live_img = None
			# print(barcodes)
			#draw = ImageDraw.Draw(data)
			for tag in tags:
				tag_info = tag.tag_id
				print(tag_info, self.pos_clock)
				# draw.polygon(barcode.polygon, outline='#e945ff')
				# point_order_ang = abs(math.degrees(math.atan2(corners[1][1] - corners[0][1], corners[1][0] - corners[0][0])) - 90)
				# print(point_order_ang)
				img_points = tag.corners
				sucess, vector_rot, vector_trans = cv2.solvePnP(self.tag_points_3d, img_points, self.camera_matrix, self.distortion_coef, flags=0)
				if sucess:
					# print(tag.corners)
					rotation_mat, _ = cv2.Rodrigues(vector_rot)
					pose_mat = cv2.hconcat((rotation_mat, vector_trans))
					euler_angles = cv2.decomposeProjectionMatrix(pose_mat)[6]
					pitch, yaw, roll = [math.radians(_) for _ in euler_angles]
					pitch = (math.asin(math.sin(pitch)))
					roll = (math.asin(math.sin(roll)))
					yaw = -(math.asin(math.sin(yaw)))
					tag_world_yaw = yaw + self.camYaw
					tag_world_pitch = pitch + self.camPitch
					tag_world_roll = roll + self.camRoll
					# print(tag_world_roll, tag_world_pitch, tag_world_yaw)
					# print(pitch, self.camPitch*180/np.pi)
					# print(roll, self.camRoll*180/np.pi)
					tag_world_x, tag_world_y, tag_world_z = self.tag_loc_tf(vector_trans[0], vector_trans[1], vector_trans[2], 
								   									   self.camX_offset, self.camY_offset, self.camZ_offset, 
								   									   self.camX, self.camY, self.camZ, 
								   									   self.camRoll, self.camPitch, self.camYaw)
					self.qr_pose_pub.publish(String('%s %s %s %s %s %s' %(tag_info, 
																	   float(tag_world_x), 
																	   float(tag_world_y), 
																	   float(tag_world_z), 
																	   float(tag_world_yaw), 
																	   float(self.pos_clock))))
			self.publish_image(np.array(data))

	def tag_loc_tf(self, x, y, z, camXoff, camYoff, camZoff, cX, cY, cZ, cRoll, cPitch, cYaw):

		self.qr_x = float(z) + camXoff
		self.qr_y = float(-x) + camYoff
		self.qr_z = float(-y) + camZoff

		#%%%%%%%% April Tag Pose Transformation %%%%%%%%%%
		
		qr_loc = self.euler_rot([cRoll, cPitch, cYaw], [self.qr_x, self.qr_y, self.qr_z])

		world_x = cX + qr_loc[0]
		world_y = cY + qr_loc[1]
		world_z = cZ + qr_loc[2]

		return world_x, world_y, world_z			

	def euler_rot(self, RPY, XYZ):
		R = RPY[0]
		P = RPY[1]
		Y = RPY[2]

		x = XYZ[0]
		y = XYZ[1]
		z = XYZ[2]

		cos_r = math.cos(R)
		cos_p = math.cos(P)
		cos_y = math.cos(Y)

		sin_r = math.sin(R)
		sin_p = math.sin(P)
		sin_y = math.sin(Y)

		rot_mat = np.array([[cos_y*cos_p, sin_r*sin_p*cos_y-cos_r*sin_y, cos_r*sin_p*cos_y+sin_r*sin_y], 
							[sin_y*cos_p, sin_r*sin_p*sin_y+cos_r*cos_y, cos_r*sin_p*sin_y-sin_r*cos_y], 
							[-sin_p, sin_r*cos_p, cos_r*cos_p]])

		loc = np.dot(rot_mat, np.array([[x], [y], [z]]))

		return loc

	def publish_image(self, imgdata):
		image_comp_temp = CompressedImage()
		image_comp_temp.header.stamp = rospy.Time.now()
		image_comp_temp.format = 'jpeg' 
		image_comp_temp.data=np.array(cv2.imencode('.jpg', imgdata)[1]).tostring()
		self.image_comp_pub.publish(image_comp_temp)


if __name__ == "__main__":
	rospy.init_node('AprilTagRead', anonymous=True)
	tag_reader = AprilTagLocator()
	tag_reader.init()
