# Dependencies
- A Ground station computer running Ubuntu 18.04 LTS with ROS installed.
    - To install ROS, follow the steps listed here: http://wiki.ros.org/melodic/Installation/Ubuntu
- A Raspberry Pi 4 using the image, battldrone_RPI4.img, provided
- A wifi network that the ground station and Raspberry Pi (sometimes reffered to as offboard computer (OBC)) are connected to.

# Setup
1. Ensure you have a Raspberry Pi 4 (OBC) setup with a micro SD card with the battledrone_RPI4.img file from this directory flashed to it.
2. For ssh connection with gitlab:
    1. Open a terminal and run `ssh-keygen -t rsa -b 2048 -C "<comment>"`
    2. To view public key for adding to gitlab account, in a terminal, run: `cat .ssh/id_rsa.pub`
2. Clone and then fork this repository to your Raspberry Pi 4:
    1. Open a terminal on the raspberry pi and run: 
    2. `sudo git clone < ssh link>`
    3. `git checkout -b <branch_name>`
    4. Rename the repository folder "BattleDrones"
4. Copy Launch config files from src/Mavros Launch files to /opt/ros/noetic/share/mavros/launch on OBC
    - `sudo cp <source_file_destination/BD_config.yaml> /opt/ros/noetic/share/mavros/launch`
    - `sudo cp <source_file_destination/BD_node.launch> /opt/ros/noetic/share/mavros/launch`
    - `sudo cp <source_file_destination/BD.launch> /opt/ros/noetic/share/mavros/launch`
5. Copy launch config files from src/USB Cam launch files to /opt/ros/noetic/share/usb_cam/launch on OBC
    - `sudo cp <source_file_destination/usb_cam-outside.launch> /opt/ros/noetic/share/usb_cam/launch`
    - `sudo cp <source_file_destination/usb_cam-inside.launch> /opt/ros/noetic/share/usb_cam/launch`
6. Setup environment on Raspberry Pi 4 and Ground Station computer.
    1. On Raspberry Pi 4
        1. `sudo nano /etc/environment`
        2. Change only the IP address numbers of the ROS_MASTER_URI address to be that of your RPI on your network. Port should be 11311 (standard for ROS).
        3. Change the ROS_IP address to be that of your RPI on your network.
        4. ctrl+s, ctrl+x to save and exit text editor.
        5. After changes are made, reboot for them to take effect: `sudo reboot`
    2. On Ground Station Computer
        1. `sudo nano /etc/environment`
        2. Change only the IP address numbers of the ROS_MASTER_URI address to be that of your RPI on your network. Port should be 11311 (standard for ROS).
        3. Change the ROS_IP address to be that of your Ground Station on your network.
        4. ctrl+s, ctrl+x to save and exit text editor.
        5. After changes are made, reboot for them to take effect: `sudo reboot`
7. Edit the file deploy_BD_outside.sh and change the IP address and password to your drone's IP address and password in the ssh commands in said file.
8. Ensure vehicle flight config parameters are safe for your flight environment and set up in accrodance with your drone by editing the file drone_config.yaml on the RPI4 OBC by running: `sudo nano <file_location>/drone_config.yaml`.

# Drone Launch
1. Open 5 terminals on your ground station computer
2. In 1 terminal execute:
    1. ssh into drone: `ssh battledrone@<drone_RPI4_ip_address>`
    2. `roscore`
        - The roscore always needs to be the first thing runing on the OBC before running any other ROS programs.
3. In terminal 2 execute:
    1. `tmux`
    2. `tmux source-file ./BattleDrones/src/tmux.conf`
        - Tmux allows us to run a shell script that will split a terminal into multiple panes for running multiple programs. This config file allows one to switch between panes using their mouse.
4. In terminal 3 execute:
    1. `cd battledrones/src`
    2. `bash deploy_BD_outside.sh`
        - After about 20 seconds, you will see the terminal split into 4 panes; Top Left = MavROS; Bottom Left = ROS node for USB cam (); Bottom Right = AprilTag Read program; Top Right = BD_mavros_coms.py ROS node for USB cam takes images from the camera and sends them out on the network as the image topics AprilTag read program will only show info when the computer vision is currently reading from an AprilTag (have to press enter on the pane for it run; will initially just show timestamps without any real information if no AprilTag to read). The BD_mavros_coms.py will continously send setpoints to the drone to keep the drone flying, without sending setpoints the drone will stop flying. Mavros is a ros node that uses mavlink (communication protocol) to send commands from the OBC to the pixhawk; ex: fly here or fly there, arm/disarm "Successfully changed to offboard mode" after pressing enter on the pane should tell us that we're good to fly
5. In terminal 4 have the command: `rostopic pub /BD_1/fly std_msgs/Bool False` typed in and ready to execute. This is command can be used during flight to cause the drone to ignore your flight control commands and return to home to land in the event the drone is not doing as it is supposed to or flying unsafe. If this fails or can't be done safely, then toggle the motor kill switch on the RC controller to try and suicide burn land the drone.
6. After all core programs are running, the drone can be flown by running your flight script, drone_navigator.py, with your own flight logic. The competitors will need to edit this file and put write their code here to autonomously fly the drone for the competition:
    - In terminal 5:
        1. ssh into drone: `ssh battledrone@<drone_RPI4_ip_address>`
        2. `cd battledrones/src`
        3. `python <drone_navigator.py>`

# Notes
 - apriltag_read.py and BD_mavros_coms.py should not be modified. Rather some user parameters can be changed in drone_config.yaml before running these programs
 ## File Descriptions 
 ### BD_mavros_coms.py
 This script creates a ROS node that communicates with the mavros node. It handles arming and disarming the drone and has functions for launching and landing the drone and deals with some of the nuances of working with Mavros. This script also does a coordinate rotation about the z-axis by the amount rot in the drone_config.yaml.
 ### apriltag_read.py
 This script creates a ROS node that reads images from the USB Camera and looks for April Tags in those images. If a tag is found, it uses the corners of the tag and an n-point perspective algorithm from opencv to estimate the tags position relative to the camera using some intrinsic camera parameters that are set in drone_congi.yaml. Finally this script transforms the tags position from the cameras reference frame into the local East/North/Up coordinate frame that the drone flies in and publishes it to the ROS topic /tag_result_out/pose
 ### deploy_BD_outside.sh
 This is a shell script that uses tmux to subdivide a terminal into four pains that each run their own terminal over ssh on the OBC for running Mavros, USB_Cam ROS, BD_mavros_coms.py, and apriltag_read.py.
 ### drone_config.yaml
 This is a yaml file containing user set parameters used in apriltag_read.py and BD_mavros_coms.py. The user should edit the parameters in this file and not edit apriltag_read.py or BD_mavros_coms.py.
 ### drone_navigator.py
 This is a template script the competitors can use and should edit for implementing their autonomous control algorithms. It has examples for publishing and subscribing to the pertinent ROS topics. This script currently creates a ROS node for navigating that would cause the drone to take off/hold for 15 seconds, fly to (1,0,1.5) and hold for 10 seconds, then reurn home and land.
