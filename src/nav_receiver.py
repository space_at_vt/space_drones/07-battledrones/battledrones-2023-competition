import rospy
from std_msgs.msg import String

class Forwarder():
	def __init__(self):
		self.pub = rospy.Publisher('/BD_1/setpoints', String, queue_size=10)

		self.listen()

	def verify(self, setpoint):
		authenticated = True # Authentication check code goes here

		if authenticated:
			self.pub.publish(setpoint)

	def listen(self):
		rospy.Subscriber('/BD_1/setpoints_auth', String, self.verify)
		rospy.spin()

def main():
	rospy.init_node('BD_1_nav_receiver', anonymous=True)

	nav_receiver = Forwarder()

if __name__ == '__main__':
	main()