# Additional Documentation
- The majority of documentation for building and flying the drone is in the Drone Build and src folders respectively.
- This folder serves to contain additional documentation for building and flying the drone as well as competition rules for the BattleDrones Competition to be held at Virginia Tech in the Spring 2023.
- The AprilTags_1_10.pdf file can be printed on a poster printer to print tags that are the size and ID's that will be used during the competition. If you rescale the tags, be sure to update the tag size parameter in the drone_config.yaml file in the src directory.
- The AprilTag source PNG's can be found here: https://github.com/AprilRobotics/apriltag-imgs along with instructions to rescale the images. The images used for this competition are the 36h11 ones.
