# BattleDrones
This repository contatins all software files needed to fly the drone through the BattleDrones race course as well as instructions, a Bill of Materials, and STLs for priting and building the drone. This repository is in support of the BattleDrones Competition hosted by Virginia Tech.

## Dependencies
- Python
- ROS
- AprilTag
- ROS/USB-Cam
- numpy
- Pillow
- opencv

## Running
Refer to src for instructions to run system on drone as well as an example drone navigation script.