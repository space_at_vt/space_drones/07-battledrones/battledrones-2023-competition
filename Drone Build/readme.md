# Build-a-Drone
## The BattleDrones quadcopter can be built by printing all body parts in the quantities listed in the Bill of Materials (BOM) and aquiring all other supporting parts.
- Print all parts in the quantities shown in the 3D Prints tab in the BOM and aquire Required parts in the COTS tab of BOM. We recommend printing parts in ABS. If it is not feasible to print them in ABS, then PLA would also work. 
- Electronics should be assembled as shown in the Harness Guide and Assembly photos
- Pixhawk initial setup using QGroundControl. Their is a video showing setup in QGroundControl for Reference:
    - Select X Quad as vehicle type under Vehicle Type Tab
    - Setup of the Pixhawk can be done by setting the Vehicle ID paramter on the pixhawk to 43 using QGroundControl. After setting the vehicle ID, the parameter file in src directory can be uploaded to the pixhawk using QGroundControl as well.
    - Calibrate vehicle sensors under calibration tab
    - Setup and calibrate radio under radio tab
- Perform a motor spin checkout ! without propellers attached ! with the battery connected using the Motors tab in QGroundControl and verify spin direction is as indicated in Harness Guide. If spin direction needs reversed, remove battery from drone and flip any two of the three wires connected to said motor.
- The drone should now be in a state that the propellers can be attached and the drone can either be flown using the RC controller which may be tricky, or using the autonomous flight control software as directed in the src directory.
